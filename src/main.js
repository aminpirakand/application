import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueResource from 'vue-resource'
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
Vue.use(VueResource);

Vue.config.productionTip = false;
Vue.http.options.root = 'https://test-c916c.firebaseio.com/'

Vue.filter('currency', (value) => {
    return value.toLocaleString() + '$';
})

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");